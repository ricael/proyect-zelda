package {
	import org.axgl.Ax;
	import org.axgl.render.AxColor;
	
	[SWF(width = "480", height = "360", backgroundColor = "#000000")]

	public class Zelda extends Ax {
		public function Zelda() {
			super(GameState);
		}
		
		override public function create():void {
			Ax.background = new AxColor(0, 0, 0);
		}
	}
}