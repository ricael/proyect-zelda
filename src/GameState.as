package {
	import org.axgl.Ax;
	import org.axgl.AxEntity;
	import org.axgl.AxGroup;
	import org.axgl.AxRect;
	import org.axgl.AxSprite;
	import org.axgl.AxState;
	import org.axgl.collision.AxCollider;
	import org.axgl.collision.AxCollisionGroup;
	import org.axgl.collision.AxGrid;
	import org.axgl.render.AxBlendMode;
	import org.axgl.render.AxColor;
	import org.axgl.text.AxText;
	import org.axgl.tilemap.AxTilemap;
	import org.axgl.util.AxRange;

	public class GameState extends AxState {
		private static var CAVE_COLLIDER:AxCollisionGroup;
		
		private var tilemap:AxTilemap;
		private var Link:Player;
		private var rect1:AxRect = null;
		private var rect2:AxRect = null;
		
		override public function create():void {
			super.create();
			
			//rect1 = new AxRect(240, 180, 240+47+background.width,240+ 144 + background.height);
			Ax.music(Resource.MUSIC);
			
			var background:AxSprite = new AxSprite(240, 180, Resource.BACKGROUND);
			background.scroll.x = background.scroll.y = 1;
			add(background);
			
			Link = new Player(355, 467,background.width ,background.height );
			Link.worldBounds = new AxRect(240, 180, 240+ background.width, 180+background.height ); // Limite del mapa para Link
			add(Link);
			
			Ax.camera.follow(Link);
			Ax.camera.bounds = new AxRect(0, 0, 500+background.width,500+ background.height);
			
			/*trace(background.width + "x" + background.height);*/
		}
		
		override public function update():void {
			super.update();
			
			/*rect2 = new AxRect(Link.left, Link.top, Link.width, Link.height);
			if (rect2.overlaps(rect1)) {
				Link.acceleration.x = 0;
			}*/
		}
	}
}
