package {
	import org.axgl.Ax;
	import org.axgl.AxRect;
	import org.axgl.AxSprite;
	import org.axgl.AxVector;
	import org.axgl.input.AxKey;
	
	

	public class Player extends AxSprite {
		
		private const MRIGHT:int = 0;
		private const MLEFT:int = 1;
		private const MFRONT:int = 2;
		private const MBACK:int = 3;
		
		private var lastDir:int;
		
		public function Player(x:Number, y:Number, worldWidth:Number, worldHeight:Number) {
			super(x, y);
			lastDir = MBACK;
			
			//Hola estoy tocando las narices!!
			
			load(Resource.LINK_WALK, 32, 32);
			addAnimation("walkFront", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 15, true);
			addAnimation("walkSide", [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 15, true);
			addAnimation("walkBack", [20, 21, 22, 23, 24, 25, 26, 27, 28, 29], 15, true);

			addAnimation("standFront", [30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 31, 32], 15, true);
			addAnimation("standSide", [34, 34, 34, 34 , 34, 34, 34, 34, 34, 34, 34, 34, 34, 34 , 34, 34, 34, 34, 34, 34, 35, 36], 15, true);
			addAnimation("standBack", [33], 1, true);
			
			maxVelocity = new AxVector(70, 70); // Velocidad Maxima X, Y
			drag.x = 300; // Slow amount, si es 0, no hay rozamiento
			drag.y = 300;
			bounds(12, 9, 9, 23);// offset determines how far to the right and down the upper left corner of the bounding box is
		}
		
		override public function update():void {
			
			// MOVIMIENTO EJE X
			if (Ax.keys.down(AxKey.RIGHT)) { // DERECHA
				acceleration.x = 500;
				if (Math.abs(velocity.x) > Math.abs(velocity.y))
				{
					facing = LEFT;
					animate("walkSide");
					lastDir = MRIGHT;
				}
			} else if (Ax.keys.down(AxKey.LEFT)) { // IZQUIERDA
				acceleration.x = -500;
				if (Math.abs(velocity.x) > Math.abs(velocity.y))
				{
					facing = RIGHT;
					animate("walkSide");
					lastDir = MLEFT;
				}
			}else {
				acceleration.x = 0;
			}
			
			// MOVIMIENTO EJE Y
			if (Ax.keys.down(AxKey.DOWN)) { // ABAJO
				acceleration.y = 500;
				if (Math.abs(velocity.x) <= Math.abs(velocity.y))
				{
					animate("walkFront");
					lastDir = MFRONT;
				}
			}else if (Ax.keys.down(AxKey.UP)) { // ARRIBA
				acceleration.y = -500;
				if (Math.abs(velocity.x) <= Math.abs(velocity.y))
				{
					animate("walkBack");
					lastDir = MBACK;
				}
			}else {
				acceleration.y = 0;
			}
			
			// ESTATICO
			if (( velocity.x == 0) && (velocity.y == 0) && !(Ax.keys.down(AxKey.DOWN)) && !(Ax.keys.down(AxKey.UP)) && !(Ax.keys.down(AxKey.LEFT)) && !(Ax.keys.down(AxKey.RIGHT))) {
				if(lastDir == MFRONT) {
					animate("standFront");
				}else if(lastDir == MBACK) {
					animate("standBack");
				}else if (lastDir == MRIGHT) {
					facing = LEFT;
					animate("standSide");
				}else if (lastDir == MLEFT) {
					facing = RIGHT;
					animate("standSide");
				}
			}
			
			super.update();
			
			/*trace("velocityX= " + velocity.x);
			trace("   velocityY= " + velocity.y + "/n");*/
		}
	}
}
