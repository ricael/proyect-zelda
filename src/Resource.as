package {
	public class Resource {

		[Embed(source = "/link_walk.png")] public static const LINK_WALK:Class;
		[Embed(source = "/grotto.png")] public static const BACKGROUND:Class;
		[Embed(source = "/12-north-hyrule-field.mp3")] public static const MUSIC:Class;
	}
}